const BackFunc = function()
   { 
     console.log(`value of display_box is ${document.getElementById("display_box").innerText} and length is ${document.getElementById("display_box").innerText.length}`);
     if(document.getElementById("display_box").innerText.length > 1)
        {
         document.getElementById("display_box").innerText = document.getElementById("display_box").innerText.substring(0,document.getElementById("display_box").innerText.length - 1);
         console.log(`value of box is now: ${document.getElementById("display_box").innerText}`);
        }
     else
        {
         console.log(`In else because display length is 1. value of box is now: ${document.getElementById("display_box").innerText}`);
         document.getElementById("display_box").innerText = "0";
        }    
   }

const disp = function(val)
   { 
     console.log(`disp function called by key press: ${val}`);
     console.log(`value of display_box is ${document.getElementById("display_box").innerText}`);
     if (document.getElementById("display_box").innerText == "0")
        {
         console.log(`inside if function, val before update is  ${val}`);
         document.getElementById("display_box").innerText = val; 
        }
     else
        {
         if (val == "=") //ready to calculate - equal sign pressed
            {
             console.log("Inside equals spotted");
             let CalcResult = CheckAndProcessCalc(document.getElementById("display_box").innerText);
             document.getElementById("display_box").innerText = CalcResult;  
            }
        else
            {    
             console.log(`inside else function val before update is  ${val}`);
             document.getElementById("display_box").innerText += val; 
             // alert(`inside function val ${val}`);
            } 
        }

     if (val == "C")
        {  //reset display to 0
         document.getElementById("display_box").innerText = "0";
        }
   } 

   const CheckAndProcessCalc = function(DisplayString)
   { 
     let total = 0;
     console.log(`Inside process calc function. value of display_box is ${DisplayString}`);

     //addition sign spotted
     if(DisplayString.indexOf("+") != -1)
       {
         total = 0;
         console.log("addition recognised");
         for (let i = 0; i < DisplayString.length; i++)
           {
            if(typeof Number(DisplayString[i]) === 'number' && DisplayString[i] != "+")
               {
                 console.log(`+++++++It is a number: ${DisplayString[i]}`);  
                total += Number(DisplayString[i]);
                console.log(`total is now ${total}`);
               }
             //console.log(scores[i]);
           } //end of for loop
       } //end of if + splotted

     //subtraction sign spotted
     if(DisplayString.indexOf("-") != -1)
        {
          total = DisplayString[0];

          console.log(`subtraction recognised, total initally set as: ${total}`);
          for (let i = 1; i < DisplayString.length; i++)
             {
              if(typeof Number(DisplayString[i]) === 'number' && DisplayString[i] != "-")
                 {
                 console.log(`-------It is a number: ${DisplayString[i]}, total = ${total}`);  
                 total -= Number(DisplayString[i]);
                 console.log(`total after sum: ${total}`);
                }
               //console.log(scores[i]);
            } //end of for loop
        } //end of if - splotted

     //multiplication sign spotted
     if(DisplayString.indexOf("*") != -1)
       {
         total = DisplayString[0];

         console.log(`multiplication recognised, total is: ${total}`);
         for (let i = 1; i < DisplayString.length; i++)
           {
             if(typeof Number(DisplayString[i]) === 'number' && DisplayString[i] != "*")
               {
                 console.log(`*******It is a number: ${DisplayString[i]}`);  
                 total = total * Number(DisplayString[i]);
                 console.log(`total after mult: ${total}, i = ${i}`);
                }
             //console.log(scores[i]);
           } //end of for loop
       } //end of if * splotted

     //Division sign spotted
     if(DisplayString.indexOf("/") != -1)
        {
         total = DisplayString[0];

         console.log(`division recognised, total is: ${total}`);
    
         if(DisplayString.indexOf("/0") != -1)
            {
            alert('Error! - dividing by zero is not allowed!. Please try again.');
            total = 0;
            return total;
            }
         for (let i = 1; i < DisplayString.length; i++)
            {
              if(typeof Number(DisplayString[i]) === 'number' && DisplayString[i] != "/")
                 {
                  console.log(`*******It is a number: ${DisplayString[i]}`);  
                  total = total/+Number(DisplayString[i]);
                  console.log(`total after mult: ${total}, i = ${i}`);
                }
             //console.log(scores[i]);
            } //end of for loop
        } //end of if / splotted

     console.log(`Value of total: ${total}`);  
     return +total.toFixed(10);
   }  //end of CheckAndProcessCalc function

