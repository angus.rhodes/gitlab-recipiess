let game_count = 1;
let draw_count = 0;
let pc_wins = 0;    
let user_wins = 0;



const getcomputerchoice = function()
{
    const pc_result =  Math.floor(Math.random()*3);
    //alert("random number is " + pc_result);
    console.log(pc_result);  

    if (pc_result == 0)
    {
     return "ROCK";
    } else if (pc_result == 1)
    {
     return "PAPER";
    } else (pc_result == 2)
    {
     return "SCISSORS";
    }
}

const play_a_round = function(pc_go, user_choice)
{
console.log(`inside play function: pc choice " ${pc_go}, user choice is: ${user_choice}`);
if ((pc_go == "ROCK" & user_choice == "PAPER") || (pc_go == "PAPER" & user_choice == "SCISSORS") || (pc_go == "SCISSORS" & user_choice == "ROCK"))
    {
     //console.log("inside R & P");
     alert(`PC= ${pc_go}, user= ${user_choice} - User wins!`);
     return "USER";
    }
else if ((pc_go == "ROCK" & user_choice == "SCISSORS") ||  (pc_go == "PAPER" & user_choice == "ROCK") ||  (pc_go == "SCISSORS" & user_choice == "PAPER"))
    {
     //console.log("inside R & S");
     alert(`PC= ${pc_go}, user= ${user_choice} - pc wins!`);
     return "PC";
    }
else if (pc_go == user_choice)
    {
     alert(`PC= ${pc_go}, user= ${user_choice} - It's a DRAW!`);
     return "DRAW";
    }

}  //end of function

const play_game = function(user_choice)
{
    let game_result ="";
    if (game_count > 5)
    {
     alert(`Game Over! - please press the 'Reset the Game' button`);
     return;
    }

    let i = 1;
     const pc_go = getcomputerchoice();
     console.log("computer: " + pc_go);
     //let user_choice = prompt("Let's play Rock Paper Scissors. Please enter your choice: ");

     console.log(`user choice is ${user_choice}`);
     game_result = play_a_round(pc_go, (user_choice.toUpperCase()));
     console.log(`game ${i} result: ${game_result}`);
     if (game_result == "DRAW")
        {
         console.log(`It's a draw! count before ${draw_count}`);
         ++draw_count;
         console.log(`It's a draw! count after ${draw_count}`);
         document.getElementById('draws').innerText = "Games drawn " + draw_count;
        }
     if (game_result == "PC")
        {
         ++pc_wins;
         document.getElementById('pc_score').innerText = "PC Score " + pc_wins;
        }
     if (game_result == "USER")
        {
         ++user_wins;
         document.getElementById('user_score').innerText = "User Score " + user_wins;
        }
     document.getElementById('games').innerText = "Games played " + game_count;
     console.log(`game count before ++ ${game_count}`);
     if (game_count == 5)
        {
          alert("Game Over!");  
        }
     ++game_count;
     console.log(`game count after ++ ${game_count}`);
     
}  //end of play game function

