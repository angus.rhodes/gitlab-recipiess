let newGridSize;

function changeColour(box)
{
  console.log("inside colour changer" + box);
  var r = Math.round(Math.random()*255);
  var g = Math.round(Math.random()*255);
  var b = Math.round(Math.random()*255);
  this.style.backgroundColor =  "rgb("+r+","+g+","+b+")";
}

function makeGrid(num)
   {
    //alert("make grid number of boxes is " + num);
   const gridSize = num * num;
   const boxsize = 960/num - 2;//size of each box in px;
   console.log("boxsize in px is: " + boxsize);
   //console.log(`value of gridSize is: ${gridSize}`);
    for(let i = 1; i <= gridSize; i++)
       { 
       //console.log(`value of i: ${i}`);
        const divElement1 = document.createElement('div');
        divElement1.classList.add("box");
        document.querySelector('.etchBox').appendChild(divElement1);
        divElement1.style.height = boxsize +'px';
        divElement1.style.width = boxsize + 'px';
        divElement1.addEventListener("mouseover", changeColour);
        }
  }    

  function resizeGrid()
  {
    newGridSize = window.prompt("Please provide a new grid size","32");
    //window.alert("Grid size is now:" + newGridSize);
    redrawGrid(newGridSize);
  }

  function redrawGrid(numberOfBoxes)
  {
    //alert("number of boxes is " + numberOfBoxes);
    document.querySelector(".etchBox").remove();
    const mainDivBox = document.createElement('div');
    mainDivBox.classList.add("etchBox");
    document.body.appendChild(mainDivBox);
    makeGrid(numberOfBoxes);
  }

 makeGrid(16);

 